<?php

namespace Drupal\Context;

/**
 * Exception thrown when attempting to modify a locked context object.
 */
class LockedException extends ContextException {}

